FROM centos:7

RUN yum install -y glibc.i686 compat-libstdc++-296.i386 \
 && yum clean all \
 && ln -s libstdc++-libc6.2-2.so.3 /usr/lib/libstdc++-libc6.1-1.so.2